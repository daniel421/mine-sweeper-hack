#include <iostream>
#include <stdio.h>
#include <tchar.h>
#include <windows.h>

#define start 0x1005340
#define end 0x10056A0

int main()
{
    DWORD size = end - start;

    DWORD dwProcessId = 0;
    DWORD dwRead = 0;

    HWND window = FindWindow(NULL, L"Minesweeper");
    if (window == NULL)
        return wprintf(L"[-] Failed to find Minesweeper process");

    GetWindowThreadProcessId(window, &dwProcessId);
    HANDLE process = OpenProcess(PROCESS_VM_READ, FALSE, dwProcessId);


    LPBYTE buffer = (LPBYTE)malloc(size);
    if (buffer == NULL)
        return wprintf(L"[-] Failed to allocated memory");

    while (true) {
        BOOL ret = ReadProcessMemory(process, (LPVOID)start, buffer, size,
            &dwRead);
        if (ret == NULL) return wprintf(L"[-] Failed to read memory");

        BYTE field = NULL;

        for (size_t i = 0, j = 0; i < size; i++, j++) {
            // Carriage return
            if (j == 0x20) {
                puts("");
                j = 0;
            }
            // Borders
            if (buffer[i] == 0x10)
                printf("#");
            // Mines
            else if (buffer[i] == 0x8F)
                printf("X");
            // Empty case
            else if (buffer[i] == 0xF)
                printf("_");
            // Flag
            else if (buffer[i] == 0x8E)
                printf("F");
            // ? mark
            else if (buffer[i] == 0xD)
                printf("?");
            // ? mark and mine under it
            else if (buffer[i] == 0x8D)
                printf("E");
            // Non explodedMines
            else if (buffer[i] == 0x8A)
            printf("x");
            // Exploded mine
            else if (buffer[i] == 0xCC)
                printf("D");
            else
                printf("%x", buffer[i]-0x40);
        }
        Sleep(1500);

        system("cls");
    }
    return 0;
}